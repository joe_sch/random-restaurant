source for https://twitter.com/_restaurant_bot

its a node app (that i wrote in an afternoon, appolgies if its... not that good), youll need credentials for google maps api and twitter api to run it, although i guess you could use it without twitter just console log the text and check to see if the images downloaded, if you want to make bots but dont know how to get started check out [this guide](https://botwiki.org/resource/tutorial/how-to-make-a-twitter-bot-the-definitive-guide)

Here's an interview I did for [a mashable article about the bot](https://mashable.com/article/random-restaurant-bot-account-twitter-rules) which you may find interesting and/or informative:

**If you don’t mind sharing, what’s your real name for the story? If you prefer I can just use your handle as well.** 

My name is Joe Schoech, my twitter is [on3ness](https://twitter.com/on3ness).

**Why did you decide to create the bot and how did you go about doing it?**

I like bots they're funny, weird, oddly artful. Their total lack of emotion, ideology, and I guess just basic being makes them soothing and interesting in contrast to the twitter forever war. I made some bots before, in fact Random Restaurant use to be a bot called [Lit Crit 911](https://twitter.com/_restaurant_bot/status/1344037986174988290). I work as a software developer so I have the requisite skills. I was bored as shit in the middle of the pandemic winter is basically how it happened. I actually started building a different bot, [@mergedrandos](https://twitter.com/mergedrandos), partially as a project to learn some new technologies. Then in the middle of doing that I got the idea for Random Restaurant. The funny thing is I spent like a week just getting the prototype of Merged Randos going, I built a site and made an iphone app and it has like 35 followers. Random Restaurant I got running in an afternoon and it has 35k.

It's a javascript/node app that mostly works by querying google's maps api. Which is to say that it's using the same data that you see when you use google maps and you tap on a restaurant and it displays user sourced photographs, except its doing it with code rather than a finger. Specifically its flow is: it randomly picks a country, then city from that country, then a restaurant from that city.

At first I had it just pick a city first rather than a country but that was returning a bunch of restaurants that're in countries with a lot of cities, which was kind of boring. So I switched it to select the country first, which is why you get a bunch of stuff on the Isle of Man, not a great culinary tradition from what I can tell, no offense. Still seeing a bunch of pictures from there has made me want to go there. There are some countries that have very few restaurants so you get reoccurring dupes with this system which imo is a funny bit. I don't check the replies often but when I do there's always someone complaining that they've seen a restaurant before.

Once it has the city, or specifically the geocordinates of the city, it sends those to google and returns a list of nearby restaurants from which one is randomly chosen, then there's a bunch of back and forth with google to get the right info, I just counted and I have seven different api calls to gather all the data which consist of a streetview image (if available, btw these suck its a picture of the front of the restaurant like 25% of the time), three or four images of the restaurant, the name and address, and a placeId which is used to make the clickable url. If it can't get all the data it needs, eg some restaurants don't have any photos, it starts over.

Btw I know, thank you everyone, that the link isn't working in the android twitter app. I feel like someone should figure that out for me? I don't know if it was twitter or android that broke it but it did use to work. I don't own an android device and don't feel like setting up an emulator because I'm not the one who broke it. UPDATE: I think it's fixed now thanks to gitlab user Richard Green. I've been meaning to post the code so people can look at if they want I just need to clean it up a little first.

The last thing it does is use twitters api to post the tweet.

**How has the account’s reach grown over time?**

I just looked at the stats and it started in april when it gained ~800 followers, may was ~2300, since then its added between 5-8k a month. I imagine it all flowed from me retweeting it at the beginning. People seemed into it right away. Which makes sense since it's pretty sick.

**Why do you think it appeals to people?**

It's prob a bunch of stuff but were I to guess it would be: obviously people like and are very interested in food; restaurants are culture not just the food but the architecture, the people, their haircuts, not sure if landscape is culture but it's compelling none the less; most of the cultural things we see online are curated and this isn't so you see different stuff than you usually would, which ends up producing an extremely different vibe, its more vernacular than some pretty instagram account; one becomes maybe more of an explorer than a consumer idk; also you get to just see some weird funny shit too like why are so many selfies of west african soldiers tagged as restaurants?

**Do you have a favorite post from the account? If so, why? If not, what do you think makes any post from the bot stand out?**

I haven't really kept track of that but I will say that the bot has convinced me that Taiwan has the best food in the world and I very much want to go there and eat it. Did you know that you can take a ferry from Taiwan to Okinawa? Free vacation concept. Speaking of chinese food, there's no google in mainland China so there are no posts from there which is sad because the food there is incredible too.

**I feel like part of the fun of the bot is that it pulls totally random pictures, often of people and live scenes,was this an intentional choice or just a happy accident of pulling the data from Google?**

Almost everything in the bot is randomized but for whatever reason I just pulled the first four photos for each restaurant from google. They're just naturally weird pictures. It prob would be better to randomize that tbh that way when you have dupes at least the pictures might be different.

**Do you answer yes or no to each tweet, as in would you go there or not? (I definitely do)**

Lol not exactly but I'm definitely checking out if it looks good.

**Anything else we should know?**

I put these countries in the list twice because I like their food: 'FR', 'US', 'ES', 'IT', 'JP', 'TW', 'TH', 'VN', 'MX', 'PT', 'KR'.
